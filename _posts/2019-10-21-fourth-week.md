---
title: 为何善行之后会有恶行
---

1. 有些名声特别好的公众人物，因为一个不克制的行为，导致现象崩溃，例如某些明星出轨、吸毒。很有可能是自控力是有限的，时刻保持着自律，让意志力消耗殆尽。

而这节将教你如何不脱离正轨，做一个自己想做的人。

## 快问快答

1. 不要被自己之前的良好行为所影响。当说到孰是孰非时，我们都毫不费力地作出符合导得标准的选择。我们只想让自己感觉良好，而这就为自己的胡作为非开了绿灯，这就是 “伪君子” 行为。所以说出的话，要想想自己是否会潜意识遵守。

   “道德许可效应” 说明了那些有明显道德标准的人能说服自己，认为出现严重的道德问题是合情合理的。例如警察对毫无抵抗能力的罪犯施以暴力，他甚至不会质疑自己的冲动。

   这个理论告诉我们，只要我们的思想中存在正反两方，好的行为就总是允许我们做一点坏事，这理论同样论证出在意志力挑战中总会不完美。

2. 不要将意志力挑战看成是道德挑战，挑战失败不说明道德差，挑战成功也不能自我感觉道德得到满足而放纵自己。如果将道德加入意志力挑战，那么你将会失去自我判断能力，看不到这些挑战有助于我们得到自己想要的东西。

   > 深入剖析：善与恶
   >
   > 这一周，试着观察你意志力挑战成功和失败的时候，你是怎么对自己和他人解释的
   >
   > - 当你意志力挑战成功的时候，你会不会告诉自己你已经很 “好” 了？当你屈服于拖延或某种诱惑的时候，你会不会告诉自己你太 “坏” 了。
   > - 你会不会以自己的善行为借口，允许自己去做些坏事？这是无害的奖励，还是阻碍了你实现更长远的意志力目标？

3. 因为多锻炼了，所以要多吃点。这是错误的逻辑，锻炼和饮食是两件事，合理的锻炼，健康的饮食。不应该因为多锻炼了而奖励自己多吃点。

   > 不要把支持目标实现的行为误认为目标本身。不是说你做了一件和你目标一致的事情，你就不会再面临危险了。注意观察一下，你是否因为认为某些积极的行为值得称赞，就忘了自己实际的目标是什么？

4. 大多数人认为，取得进步会刺激我们获得更大的进步。但是心理学家知道，我们总是把进步当作放松的借口。这和复习是一个道理，当你自我感觉复习良好，那么你很有可能会放慢复习进度。成功会让你自我满足，导致放纵，不要被胜利冲昏了头脑就是这个道理。

5. 很多人喜欢列清单，但是其实列完清单后你会想象做完后的场景，导致自己自我满足，从而又放纵自己...

   所以，以后不应该想着 “我今天做到了”，而是想着 “我离真正的目标还有多远”。

   > 意志力试验：取消许可，牢记理由
   >
   > 如何关注对自己的承诺，而不是关注单纯的进步？
   >
   > 当你发现自己在用曾经的善行给现在的放纵作辩护时，停下来想一想，你当时为什么能拒绝诱惑。

6. 只是 “想” 去锻炼的人很可能晚餐吃得更多。我们想到未来的选择时，就会很容易犯下大错。我们不断期望每天能作出和今天不同的选择，但这种乐观精神会让我们在今天放纵自己。

   当知道自己未来还有机会的人，往往不会那么努力。这也说明了背水一战的人为什么能这么勇猛，因为没有其他机会了。

   > 深入剖析：你是在向昨天赊账吗？
   >
   > 当你要作出与意志力挑战有关的决定时，注意一下，你脑海中是否闪过了 “未来再好好表现” 的承诺。你是不是告诉自己，明天会弥补今天的过错？这对你当下的自控有什么影响？一直保持关注，从今天一直关注到明天。你是不是真的做到了你所说的？或者，“今天放纵，明天改变” 的循环是不是又开始了？

7. 乐观主义精神总是会把未来的意志力挑战想象得很好，但实际总是低于预期。

   > 意志力试验：明天和今天毫无区别
   >
   > 试着逐渐减少行为的变化性。把你今天作的每个决定都看成是对今后每天的承诺。因此，不要问自己 “我现在想不想吃这块糖？”，而是问自己 “我想不想在一年里每天下午都吃一块糖？” 或者，你明知道应该做一件事情却拖延不做时，不要问自己 “我是想今天做还是明天做？”，而是问自己 “我是不是想承担永远拖延下去的恶果？”

8. 给自己每天的行为做一个规则，固定的时间做固定的事，这样就不会因为想着明天而放纵今天了。

   > 你的生活有没有这样一个规则，来帮你结束内心的挣扎？

9. 当使你放纵的东西和使你觉得品德高尚的东西同时出现时，就会产生光环效应，让你的罪恶感消失不见。“自我感觉” 常常会取代尝试判断。

   > 深入剖析：你正在被光环笼罩吗？
   >
   > 你会不会因为关注一个事物最有益的品质，而允许自己沉溺于它？有没有什么 “神奇词语” 会给你放纵的许可？比如 “买一送一”。“全天然”、“淡”、"公平贸易"、“有机” 或 “为了慈善”。这一周，看看你是否被那些破坏长远目标的光环所笼罩。

10. 打折总是能勾起人的购买欲，因为总想着自己省钱（我真是购物天才）；何不反过来想想你花了多少钱？

    > 当 “光环效应” 影响到你的意志力挑战时，你需要找到最具体的测量标准（比如卡路里、花费、消耗或浪费的时间），以此来判断这个选择是否和你的目标相符。

11. 小的 “绿色” 行为会降低消费者和商家的罪恶感，允许他们做出伤害性更大的事。弥补之前的愧疚，很可能接着做其他让你更愧疚的事。

    > 深入剖析：你觉得自己是谁？
    >
    > 当你思考自己的意志力考验时，你觉得哪部分的你像 “”真实“ 的你？是那个想追求目标的你，还是那个需要被控制的你？你是更认同自己的冲动和欲望，还是更认同自己的长期目标和价值观？当你想到你的意志力挑战时，你觉得自己是能成功的人吗？还是说，你觉得自己需要被彻底压抑，完善或改变？

不要要所有的意志力挑战都放在道德标准的框架中。我们认同的是目标本身，而不是道德光环。

## 总结

核心思想：当我们将意志力挑战看成衡量道德水平的标准时，善行就会允许我们做坏事。为了能更好地自控，我们需要忘掉美德，关注目标和价值观。

### 深入剖析

- 善与恶：当你的意志力挑战成功时，你会不会告诉自己你很 ”好“，然后允许自己做一些 ”坏“ 事。
- 你是否在向明天赊账？你是不是告诉自己明天会弥补今天的过错？如果是这样的话，你是否真的弥补上了？光环效应：你是不是只看到了坏东西好的一面，如折扣省钱、零脂肪、保护环境？
- 你觉得自己是谁？当你看到你的意志力挑战时，你觉得哪部分的你才是 ”真实“ 的你？是想追求目标的你，还是需要被控制的你？

### 意志力试验

- 明天和今天毫无区别。当你想改变行为的时候，试着减少行为的变化性，而不是减少某种行为。
- 取消许可，牢记理由。下一回，当你发现自己在用曾经的善行为放纵辩护的时候，停下来想一想你做 ”好“ 事的原因，而不是你应不应该得到奖励。
